# Что это за репо

Переведенный на Jinja [шаблон welcome](https://www.figma.com/file/c7ApYnDfVORCsAYM1l8mnu/BrightEdge-S3%3A-Platform-Emails?node-id=39%3A120)

Использована немного доработанная верстка из репо https://github.com/huntercront/br-emails

# Как запустить

```
$ git clone 
$ pip install Jinja2
$ python test.py
```

# Логика шаблонизации

Логика сейчас очень простая

Если в `test.json` есть параметр domain - используется версия шаблона письма где есть сайт клиента.

Если они планируют A/B тест и надо не привязываться к данным то как вариант выбирать шаблон через rand, типа

```python
import random
if random.randint(0,1):
    print("First template")
else:
    print("Second template") 
```

Но т.к. я не знаю как у конечного клиента реализовано A/B тестирование использовать этот код в Jinja не стал

# Как отправить тестовое письмо?

```
$ echo "Subject: sendmail test" | sendmail -v pavel.p.serikov@gmail.com # просто проверить что sendmail работает
cat test.html | sendmail -v pavel.p.serikov@gmail.com
```

но вообще если клиент отправляет письма через SendGrid - и для тестирования лучше использовать эту же платформу. У них вроде есть [норм API](https://sendgrid.com/docs/api-reference/)

# Структура проекта

Все шаблоны jinja в папке `j2`

`img.yml` - конфиг для быстрой замены картинок

Различающиеся части письма поместил в разные файлы.

Т.к. никаких рекомендаций по python code style нет то имя переменной шаблона совпадает с именем куска кода (в терминах jinja - макроса), чтоб было меньше путаницы.
Поэтому в коде может быть такая тавтология, не пугайтесь

```
{% import 'increase.j2' as INCREASE -%}
{% import 'increase_w_domain.j2' as INCREASE_W_DOMAIN -%}
...
{% if domain %}
  {{ INCREASE_W_DOMAIN.INCREASE_W_DOMAIN(domain, get_started_link) }}
{% else %}
  {{ INCREASE.INCREASE(get_started_link) }}
{% endif %}
```

base64 кодирование

data:image/png;

# Целевые версии

Python 2.7
Jinja2==2.10

Проверял нативно на версиях

```
$ python --version
Python 2.7.17
$ pip freeze | grep Jinja2
Jinja2==2.10.3
```

Если необходимо могу сделать docker image для быстрого запуска