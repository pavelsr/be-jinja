import os
import jinja2 as j2
import json
import yaml
import base64

# defaults
class Config:
  img_type = "remote" # ENUM "local", "remote", "base64"
  includes_path = "j2"
  img_folder = "img"
  img_cnf = "img.yml"
  sample_data = "test.json"
  main_tpl = "main.j2"
  rendered_tpl = "test.html"
  
def resolve_img(images_obj, Config):
    for key, value in images_obj.items():
        print "Process image "+key
        if Config.img_type == "remote":
            images_obj[key] = value["remote"];
        elif Config.img_type == "base64":
            filename, file_extension = os.path.splitext(value["local"])
            file_extension = file_extension.replace(".", "")
            file = open( Config.img_folder + "/" + value["local"], "r" ).read();
            # data:image/png;base64,<content>
            images_obj[key] = "data:image/" + file_extension + ";base64," + base64.standard_b64encode(file)
        else:
            images_obj[key] = Config.img_folder + "/" + value["local"];

file_json = open(Config.sample_data, "r")
data_json = json.load(file_json)
print type(data_json)
 
def get_mail_html_body(Config, sample_data): 
    file_img_cnf = open(Config.img_cnf, "r")
    data_img_cnf = yaml.load(file_img_cnf)
    resolve_img(data_img_cnf, Config)
    sample_data['img'] = data_img_cnf
    env = j2.Environment(loader=j2.FileSystemLoader(Config.includes_path))
    template = env.get_template(Config.main_tpl)
    with open(Config.rendered_tpl, "w") as f:
        f.write(template.render(data_json))
    print "Rendered to "+Config.rendered_tpl

# some king of unit test
Config.rendered_tpl = "rendered_mail_html/DataCube-Welcome-1.html"
get_mail_html_body(Config, data_json)

Config.rendered_tpl = "rendered_mail_html/DataCube-Welcome-1-base64img.html"
Config.img_type = "base64"
get_mail_html_body(Config, data_json)


del data_json['domain']

Config.rendered_tpl = "rendered_mail_html/DataCube-Welcome-2.html"
get_mail_html_body(Config, data_json)

Config.rendered_tpl = "rendered_mail_html/DataCube-Welcome-2-base64img.html"
Config.img_type = "base64"
get_mail_html_body(Config, data_json)
